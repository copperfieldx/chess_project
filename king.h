#ifndef KING_H
#define KING_H

#include "chessfigure.h"

/*!
 *\brief Klasa odpowiada za reprezentację figury szachowej króla
 *
 * Klasa odpowiada za stworzenie obiektu klasy King,
 * ustawienie grafik reprezentujących figurę króla,
 * a także sprawdzanie dozwolonych ruchów w danej sytuacji na planszy
 */
class King : public ChessFigure
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy King
     * \param[in] side kolor figury szachowej
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    King(Color side, QGraphicsItem *parent = 0);

    /*!
     * \brief Funkcja odpowiedzialna za ustawienie grafik reprezentujących
     * figurę króla
     */
    void setIcon();

    /*!
     * \brief Funkcja odpowiedzialna za sprawdzenie dozwolonych ruchów króla
     * w danej sytuacji na planszy
     */
    void checkValidMoves();
};

#endif // KING_H
