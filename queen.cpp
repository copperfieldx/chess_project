#include "queen.h"
#include "game.h"

extern Game* game;

Queen::Queen(Color side, QGraphicsItem *parent):ChessFigure(side, parent)
{
    setIcon();
}


void Queen::setIcon()
{
    if(this->color==white)
    {
        setPixmap(QPixmap(":/chess_figure_icons/queen_white.png"));
    }
    else
    {
        setPixmap(QPixmap(":/chess_figure_icons/queen_black.png"));
    }

}

void Queen::checkValidMoves()
{
    possibleMoves.clear();

    int col = this->currentBox->colPosition;
    int row = this->currentBox->rowPosition;

    //for left turn
    for(int i=col-1; i>=0; i--)
    {
        if(game->fields[i][row]->getCurrentChessFigure() != NULL)
        {
            if(game->fields[i][row]->getCurrentChessFigure()->getColor()==this->getColor())
            {
                break;
            }
            else
            {
                possibleMoves.push_back(game->fields[i][row]);
                break;
            }

        }
        else
        {
            possibleMoves.push_back(game->fields[i][row]);
        }

    }


    //for right turn
  for(int i=col+1; i<=7;i++)
  {
      if(game->fields[i][row]->getCurrentChessFigure()!=NULL)
      {
          if(game->fields[i][row]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[i][row]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[i][row]);
      }

  }


  //for down turn
  for (int j=row+1; j<=7; j++)
  {
      if(game->fields[col][j]->getCurrentChessFigure()!=NULL)
      {
          if(game->fields[col][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[col][j]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[col][j]);
      }
  }

  //for up turn
  for(int j =row-1; j>=0; j--)
  {
      if(game->fields[col][j]->getCurrentChessFigure()!=NULL)
      {
          if(game->fields[col][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[col][j]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[col][j]);
      }

  }

  //up left
  for(int i=col-1, j=row-1; i>=0 && j>=0; i--,j--)
  {

      if(game->fields[i][j]->getCurrentChessFigure() != NULL)
      {
          if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[i][j]);
              break;
          }

      }
      else
      {
          possibleMoves.push_back(game->fields[i][j]);
      }

  }

  //up right
  for(int i=col+1,j=row-1; i<=7 && j>=0; i++,j--)
  {

      if(game->fields[i][j]->getCurrentChessFigure() != NULL)
      {
          if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[i][j]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[i][j]);
      }

  }

  //down right
  for(int i=col+1,j=row+1; i<=7 && j<=7; i++,j++)
  {

      if(game->fields[i][j]->getCurrentChessFigure() != NULL)
      {
          if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
               possibleMoves.push_back(game->fields[i][j]);
               break;
          }
      }
      else
      {
           possibleMoves.push_back(game->fields[i][j]);
      }

  }


  //down left
  for(int i=col-1,j=row+1; i>=0 && j<=7; i--,j++)
  {

      if(game->fields[i][j]->getCurrentChessFigure() != NULL)
      {
          if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[i][j]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[i][j]);
      }
  }
}
