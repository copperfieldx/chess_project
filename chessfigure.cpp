#include "chessfigure.h"
#include "game.h"
#include "chessbox.h"
#include <vector>

using namespace std;

extern Game* game;

ChessFigure::ChessFigure(Color side, QGraphicsItem *parent):QGraphicsPixmapItem(parent)
{
    this->color = side;
    isChosen = false;
    isFirstMove = true;
}

ChessBox* ChessFigure::getCurrentBox()
{
    return currentBox;
}

void ChessFigure::setCurrentBox(ChessBox *chessbox)
{
    this->currentBox = chessbox;
}

ChessFigure::Color ChessFigure::getColor()
{
    return color;
}

void ChessFigure::setColor(Color color)
{
    this->color = color;
}

bool ChessFigure::getIsChosen()
{
    return isChosen;
}

void ChessFigure::setIsChosen(bool value)
{
    isChosen = value;
}

vector<ChessBox *> ChessFigure::getPossibleMoves()
{
    return possibleMoves;
}

bool ChessFigure::getIsFirstMove()
{
    return isFirstMove;
}

void ChessFigure::setIsFirstMove(bool value)
{
    isFirstMove = value;
}

bool ChessFigure::checkIsInPossibleMoves(ChessBox* box)
{
    for(vector<ChessBox*>::iterator it=possibleMoves.begin(); it!=possibleMoves.end(); ++it)
    {

        if(box == *it)
        {
            return true;
        }
    }
    return false;
}

void ChessFigure::hideValidMoves()
{
    for(vector<ChessBox*>::iterator it=possibleMoves.begin(); it!=possibleMoves.end(); ++it)
    {
        (*it)->changeColor((*it)->getBasicColor());
    }
    possibleMoves.clear();
}

void ChessFigure::showValidMoves()
{
    for(vector<ChessBox*>::iterator it=possibleMoves.begin(); it!=possibleMoves.end(); ++it)
    {
        if((*it)->getCurrentChessFigure()!=NULL)
        {
            (*it)->changeColor(Qt::red);
        }
        else
        {
            (*it)->changeColor(Qt::green);
        }
    }
}
