#ifndef QUEEN_H
#define QUEEN_H

#include "chessfigure.h"

/*!
 *\brief Klasa odpowiada za reprezentację figury szachowej królowej
 *
 * Klasa odpowiada za stworzenie obiektu klasy Queen,
 * ustawienie grafik reprezentujących figurę królowej,
 * a także sprawdzanie dozwolonych ruchów w danej sytuacji na planszy
 */
class Queen : public ChessFigure
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy Queen
     * \param[in] side kolor figury szachowej
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    Queen(Color side, QGraphicsItem *parent = 0);

    /*!
     * \brief Funkcja odpowiedzialna za ustawienie grafik reprezentujących
     * figurę królowej
     */
    void setIcon();

    /*!
     * \brief Funkcja odpowiedzialna za sprawdzenie dozwolonych ruchów królowej
     *  w danej sytuacji na planszy
     */
    void checkValidMoves();
};

#endif // QUEEN_H
