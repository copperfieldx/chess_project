#ifndef PAWN_H
#define PAWN_H

#include "chessfigure.h"

/*!
 *\brief Klasa odpowiada za reprezentację figury szachowej pionka
 *
 * Klasa odpowiada za stworzenie obiektu klasy Pawn,
 * ustawienie grafik reprezentujących figurę pionka,
 * a także sprawdzanie dozwolonych ruchów w danej sytuacji na planszy
 */
class Pawn : public ChessFigure
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy Pawn
     * \param[in] side kolor figury szachowej
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    Pawn(Color side, QGraphicsItem *parent = 0);

    /*!
     * \brief Funkcja odpowiedzialna za ustawienie grafik reprezentujących
     * figurę pionka
     */
    void setIcon();

    /*!
     * \brief Funkcja odpowiedzialna za sprawdzenie dozwolonych ruchów pionka
     * w danej sytuacji na planszy
     */
    void checkValidMoves();
};

#endif // PAWN_H
