#ifndef KNIGHT_H
#define KNIGHT_H

#include "chessfigure.h"

/*!
 *\brief Klasa odpowiada za reprezentację figury szachowej skoczka
 *
 * Klasa odpowiada za stworzenie obiektu klasy Knight,
 * ustawienie grafik reprezentujących figurę skoczka,
 * a także sprawdzanie dozwolonych ruchów w danej sytuacji na planszy
 */
class Knight : public ChessFigure
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy Knight
     * \param[in] side kolor figury szachowej
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    Knight(Color side, QGraphicsItem *parent = 0);

    /*!
     * \brief Funkcja odpowiedzialna za ustawienie grafik reprezentujących
     * figurę skoczka
     */
    void setIcon();

    /*!
     * \brief Funkcja odpowiedzialna za sprawdzenie dozwolonych ruchów skoczka
     * w danej sytuacji na planszy
     */
    void checkValidMoves();
};

#endif // KNIGHT_H
