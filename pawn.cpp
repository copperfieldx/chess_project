#include "pawn.h"
#include "game.h"

extern Game* game;

Pawn::Pawn(Color side, QGraphicsItem *parent):ChessFigure(side, parent)
{
    setIcon();
}

void Pawn::setIcon()
{
    if(this->color==white)
    {
        setPixmap(QPixmap(":/chess_figure_icons/pawn_white.png"));
    }
    else
    {
        setPixmap(QPixmap(":/chess_figure_icons/pawn_black.png"));
    }

}

void Pawn::checkValidMoves()
{
    possibleMoves.clear();

    int col = this->currentBox->colPosition;
    int row = this->currentBox->rowPosition;


    if(this->getColor() == Color::white)
    {
        //if oponent diagonally
        if(col>0 && row<7 && game->fields[col-1][row+1]->getCurrentChessFigure()!=NULL && game->fields[col-1][row+1]->getCurrentChessFigure()->getColor() == Color::black)
        {
                possibleMoves.push_back(game->fields[col-1][row+1]);
        }

        //if oponent diagonally
        if(col<7 && row<7 && game->fields[col+1][row+1]->getCurrentChessFigure()!=NULL && game->fields[col+1][row+1]->getCurrentChessFigure()->getColor() == Color::black)
        {
                 possibleMoves.push_back(game->fields[col+1][row+1]);
        }

        //if field empty in front of figure
        if(row<7 && game->fields[col][row+1]->getCurrentChessFigure() == NULL)
        {
            possibleMoves.push_back(game->fields[col][row+1]);
        }

        //if field empty, first move(another from the next moves)
        if(getIsFirstMove()==true && row<7 && game->fields[col][row+2]->getCurrentChessFigure()==NULL)
        {
            possibleMoves.push_back(game->fields[col][row+2]);
        }

    }

    if(this->getColor() == Color::black)
    {
        //if oponent diagonally
        if(col>0 && row<7 && game->fields[col-1][row-1]->getCurrentChessFigure()!=NULL && game->fields[col-1][row-1]->getCurrentChessFigure()->getColor() == Color::white)
        {
                possibleMoves.push_back(game->fields[col-1][row-1]);
        }

        //if oponent diagonally
        if(col<7 && row<7 && game->fields[col+1][row-1]->getCurrentChessFigure()!=NULL && game->fields[col+1][row-1]->getCurrentChessFigure()->getColor() == Color::white)
        {
                 possibleMoves.push_back(game->fields[col+1][row-1]);
        }

        //if field empty in front of figure
        if(row<7 && game->fields[col][row-1]->getCurrentChessFigure() == NULL)
        {
            possibleMoves.push_back(game->fields[col][row-1]);
        }
        //if field empty, first move(another from the next moves)
        if(getIsFirstMove()==true && row<7 && game->fields[col][row-2]->getCurrentChessFigure()==NULL)
        {
            possibleMoves.push_back(game->fields[col][row-2]);
        }

    }

}
