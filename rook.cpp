#include "rook.h"
#include "game.h"

extern Game* game;

Rook::Rook(Color side, QGraphicsItem *parent):ChessFigure(side, parent)
{
    setIcon();
}


void Rook::setIcon()
{
    if(this->color==white)
    {
        setPixmap(QPixmap(":/chess_figure_icons/rook_white.png"));
    }
    else
    {
        setPixmap(QPixmap(":/chess_figure_icons/rook_black.png"));
    }

}

void Rook::checkValidMoves()
{
    possibleMoves.clear();

    int col = this->currentBox->colPosition;
    int row = this->currentBox->rowPosition;


    //for left turn
    for(int i=col-1; i>=0; i--)
    {
        if(game->fields[i][row]->getCurrentChessFigure() != NULL)
        {
            if(game->fields[i][row]->getCurrentChessFigure()->getColor()==this->getColor())
            {
                break;
            }
            else
            {
                possibleMoves.push_back(game->fields[i][row]);
                break;
            }

        }
        else
        {
            possibleMoves.push_back(game->fields[i][row]);
        }

    }


    //for right turn
  for(int i=col+1; i<=7;i++)
  {
      if(game->fields[i][row]->getCurrentChessFigure()!=NULL)
      {
          if(game->fields[i][row]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[i][row]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[i][row]);
      }

  }


  //for down turn
  for (int j=row+1; j<=7; j++)
  {
      if(game->fields[col][j]->getCurrentChessFigure()!=NULL)
      {
          if(game->fields[col][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[col][j]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[col][j]);
      }
  }

  //for up turn
  for(int j =row-1; j>=0; j--)
  {
      if(game->fields[col][j]->getCurrentChessFigure()!=NULL)
      {
          if(game->fields[col][j]->getCurrentChessFigure()->getColor()==this->getColor())
          {
              break;
          }
          else
          {
              possibleMoves.push_back(game->fields[col][j]);
              break;
          }
      }
      else
      {
          possibleMoves.push_back(game->fields[col][j]);
      }

  }

}


