#ifndef CHESSFIGURE_H
#define CHESSFIGURE_H

#include <QGraphicsPixmapItem>
#include "chessbox.h"
#include <QGraphicsSceneMouseEvent>
#include <vector>

using namespace std;

class ChessBox;

/*!
 * \brief Klasa abstrakcyjna odpowiedzialna za reprezentację figury szachowej
 *
 * Klasa jest klasą abstrakcyjną i z niej dziedziczą poszczególne figury szachowe
 * (pionek, król, królowa, goniec, skoczek, wieża)
 */

class ChessFigure:public QGraphicsPixmapItem
{
public:

    /*!
     * \brief Typ wyliczeniowy do reprezentacji dwóch kolorów figur szachowych
     */
    enum Color
    {
        white,
        black
    };

    /*!
     * \brief Konstruktor obiektu z którego dziedziczą klasy pochodne
     * \param[in] side kolor pionka szachowego
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    ChessFigure(Color side, QGraphicsItem *parent = 0);

    /*!
     * \brief Funkcja zwracająca aktualne pole szachowe na którym stoi figura
     * \return aktualne pole szachowe na którym stoi figura lub NULL
     */
    ChessBox *getCurrentBox();

    /*!
     * \brief Funkcja zmieniające aktualne pole szachowe danego obiektu
     * \param[in] chessbox pole szachowe do przypisania danej figurze
     */
    void setCurrentBox(ChessBox* chessbox);

    /*!
     * \brief Funkcja zwracająca aktualny kolor figury szachowej
     * \return aktualny kolor figury szachowej(zgodnie z typem wyliczeniowym Color)
     */
    Color getColor();

    /*!
     * \brief Funkcja ustawiająca kolor danej figury szachowej
     * \param[in] color kolor figury szachowej, który chcemy ustawić
     */
    void setColor(Color color);\

    /*!
     * \brief Funkcja zwracająca informację czy dana figura jest aktualnie wybrana w grze
     * \return true jeśli figura jest aktualnie wybrana, false jeśli nie
     */
    bool getIsChosen();

    /*!
     * \brief Funkcja ustawiająca informację czy dana figura jest aktualnie wybrana
     * \param[in] value wartość true/false czy dana figura jest aktualnie wybrana
     */
    void setIsChosen(bool value);

    /*!
     * \brief Funkcja zwracająca tablicę możliwych ruchów w danej sytuacji na planszy
     * \return tablica możliwych ruchów w danej sytuacji na planszy
     */
    vector <ChessBox*> getPossibleMoves();

    /*!
     * \brief Funkcja zwracająca informację czy to pierwszy ruch danej figury na planszy
     * \return true jeśli to pierwszy ruch danej figury, false jeśli nie
     */
    bool getIsFirstMove();

    /*!
     * \brief Funkcja ustawiająca informacę czy jest to pierwszy ruch danej figury na planszy
     * \param[in] value wartość true/false czy jest to pierwszy ruch danej figury na planszy
     */
    void setIsFirstMove(bool value);

    /*!
     * \brief Funkcja sprawdzająca czy dane pole szachowe znajduje się w obrębie
     * prawidłowych ruchów danej figury w danym momencie gry
     * \param[in] box pole szachowe danej szachownicy
     * \return true jeśli pole znajduje się w obrębie możliwych ruchów danej figury
     * w danym momencie gry, false jeśli nie
     */
    bool checkIsInPossibleMoves(ChessBox* box);

    /*!
     * \brief Funkcja ukrywająca dozwolone ruchy danego pionka w interfejsie użytkownika
     */
    void hideValidMoves();

    /*!
     * \brief Funkcja pokazująca dozwolone ruchy danego pionka w interfejsie użytkownika
     */
    void showValidMoves();

    /*!
     * \brief Funkcja wirtualna do implementacji w klasach pochodnych,
     * służy do ustawienia ikony odpowiadającej danej figurze szachowej
     */
    virtual void setIcon() = 0;

    /*!
     * \brief Funkcja wirtualna do implementacji w klasach pochodnych,
     * służy do sprawdzania możliwych ruchów danej figury w danym momencie gry
     */
    virtual void checkValidMoves() = 0;

protected:

    /*!
     * \brief Zmienna odpowiadająca za przechowywanie informacji o aktualnym
     * polu szachownicy na którym znajduje się dana figura
     */
    ChessBox *currentBox;

    /*!
     * \brief Zmienna odpowiadająca za przechowywanie informacji o kolorze
     * danej figury szachowej
     */
    Color color;

    /*!
     * \brief Zmienna odpowiadająca za przechowywanie informacji o tym,
     * czy dana figura jest aktualnie wybraną w grze
     */
    bool isChosen;

    /*!
     * \brief Tablica zawierająca wszystkie aktualne możliwe pola na które może
     * wykonać ruch dana figura w danym momencie gry
     */
    vector <ChessBox* > possibleMoves;

    /*!
     * \brief Zmienna odpowiadająca za przechowywanie informacji o tym,
     * czy jest to pierwszy ruch danej figury
     */
    bool isFirstMove;
};

#endif // CHESSFIGURE_H
