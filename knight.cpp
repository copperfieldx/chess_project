#include "knight.h"
#include "game.h"

extern Game* game;

Knight::Knight(Color side, QGraphicsItem *parent):ChessFigure(side, parent)
{
    setIcon();
}

void Knight::setIcon()
{
    if(this->color==white)
    {
        setPixmap(QPixmap(":/chess_figure_icons/knight_white.png"));
    }
    else
    {
        setPixmap(QPixmap(":/chess_figure_icons/knight_black.png"));
    }

}

void Knight::checkValidMoves()
{
    possibleMoves.clear();

    int col = this->currentBox->colPosition;
    int row = this->currentBox->rowPosition;

    int colOffsets[8]={-2,+2,-2,+2,-1,+1,-1,+1};
    int rowOffsets[8]={-1,-1,+1,+1,-2,-2,+2,+2};



    for(int i=0;i<8;i++)
    {
        int colToCheck = col + colOffsets[i];
        int rowToCheck = row + rowOffsets[i];

        if(colToCheck>=0 && colToCheck<=7 && rowToCheck>=0 && rowToCheck<=7)
        {
            if((game->fields[colToCheck][rowToCheck]->getCurrentChessFigure() != NULL && game->fields[colToCheck][rowToCheck]->getCurrentChessFigure()->getColor()!=this->getColor()) || game->fields[colToCheck][rowToCheck]->getCurrentChessFigure() == NULL)
            {
                possibleMoves.push_back(game->fields[colToCheck][rowToCheck]);
            }

        }

    }

}

