#ifndef GAME_H
#define GAME_H
#include <QGraphicsView>
#include <QGraphicsScene>
#include "chessboard.h"
#include "chessbox.h"
#include "chessfigure.h"
#include <string>
using namespace std;

class ChessFigure;


/*!
 * \brief Klasa odpowiedzialna za reprezentację pojedynczej gry
 *
 * Klasa odpowiada za stworzenie obiektu klasy Game,
 * wyświetlenie interfejsu gry, dodawanie i usuwanie elementów z widoku,
 * zarządzanie ruchami graczy i sprawdzanie warunków zakończenia gry
 */
class Game:public QGraphicsView
{
    Q_OBJECT
public:

    /*!
     * \brief Klasa odpowiedzialna za stworzenie klasy Game
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    Game(QWidget* parent = 0);

    /*!
     * \brief Funkcja odpowiedzialna za wyświetlenie gry, narysowanie szachownicy
     * oraz wyświetlenie figur na planszy
     */
    void displayGame();

    /*!
     * \brief Funkcja odpowiedzialna za dodanie i wyświetlenie nowego elementu
     * w interfejsie użytkownika
     * \param[in] item element graficzny do dodania na ekranie
     */
    void addToScene(QGraphicsItem *item);

    /*!
     * \brief Funkcja odpowiedzialna za usunięcie elementu z interfejsu użytkownika
     * \param[in] item element graficzny do usunięcia z ekranu
     */
    void removeFromScene(QGraphicsItem *item);

    /*!
     * \brief Funkcja odpowiedzialna za ustalanie ruchu graczy
     * \param[in] color kolor gracza, którego ruch nastąpi
     */
    void setWhichTurn(ChessFigure::Color color);

    /*!
     * \brief Funkcja odpowiedzialna za zwrócenie informacji,
     * czyj ruch jest aktualnie wykonywany
     * \return kolor gracza, którego ruch jest aktualnie wykonywany
     */
    ChessFigure::Color getWhichTurn();

    /*!
     * \brief Zmiana ruchu na kolor gracza przeciwnego
     */
    void changeWhichTurn();

    /*!
     * \brief Sprawdza warunek zakończenia rozgrywki.
     * Wywoływana w momencie zbicia figury. Jeśli podana na wejściu figura to król
     * oznacza to zakończenie rozgrywki i wygraną gracza przeciwnego niż kolor króla.
     * \param[in] figure Figura do sprawdzenia po zbiciu, czy jest to król.
     */
    void verifyEndOfTheGame(ChessFigure* figure);

    /*!
     * \brief Funkcja odpowiedzialna za wyczyszczenie interfejsu graficznego użytkownika
     * ze wszystkich elementów graficznych.
     */
    void clearAll();

    /*!
     * \brief Wskaźnik na element, który aktualnie wykonuje ruch
     */
    ChessFigure* currentMoveFigure;

    /*!
     * \brief Wskaźnik na tablicę szachownicy
     */
    ChessBox* fields[8][8];

private:

    /*!
     * \brief Wskaźnik na interfejs graficzny gry
     */
    QGraphicsScene* gameScene;

    /*!
     * \brief Wskaźnik na szachownicę biorącą udział w aktualnej rozgrywce
     */
    ChessBoard* chessboard;

    /*!
     * \brief Informacja o tym, który gracz wykonuje aktualnie ruch
     */
    ChessFigure::Color whichTurn;

    /*!
     * \brief Wskaźnik na element graficzny(tekstowy) informujący o tym,
     * kto aktualnie wykonuje ruch
     */
    QGraphicsTextItem *whichTurnText;

    /*!
     * \brief Wskaźnik na element graficzny(tekstowy) informujący o zakończeniu
     * rozgrywki
     */
    QGraphicsTextItem *gameOverText;
};

#endif // GAME_H
