#include "chessboard.h"
#include "chessbox.h"
#include "game.h"
#include "bishop.h"
#include "king.h"
#include "knight.h"
#include "pawn.h"
#include "queen.h"
#include "rook.h"
#include <QBrush>
#include <vector>

using namespace std;

extern Game *game;

ChessBoard::ChessBoard()
{
    setWhite();
    setBlack();

}


void ChessBoard::drawChessBoard(int x, int y)
{
    int boxSize = 100;
    for(int i=0; i<8; i++)
    {
        for(int j=0;j<8;j++)
        {
            ChessBox *box = new ChessBox();
            game->fields[j][i] = box;
            box->colPosition = j;
            box->rowPosition = i;
            box->setPos(x+boxSize*j,y+boxSize*i);

            if((i%2==1 && j%2==1) || (i%2==0 && j%2==0))
            {
                box->setBasicColor(Qt::white);
                box->changeColor(Qt::white);
            }
            else
            {
                box->setBasicColor(Qt::gray);
                box->changeColor(Qt::gray);
            }

            game->addToScene(box);
        }
    }

}


void ChessBoard::addChessFigure()
{
    int k=0;
    int l=0;
    for(int i=0; i<8; i++)
    {

        for (int j=0; j<8; j++)
        {
            ChessBox *box = game->fields[j][i];
            if(i<=1)
            {
                box->setFigure(white[k]);
                game->addToScene(white[k]);
                k++;
            }
            if(i>=6)
            {
                box->setFigure(black[l]);
                game->addToScene(black[l]);
                l++;
            }
        }
    }
}

void ChessBoard::setWhite()
{
    ChessFigure *figure;

    figure = new Rook(ChessFigure::white);
    white.push_back(figure);
    figure = new Knight(ChessFigure::white);
    white.push_back(figure);
    figure = new Bishop(ChessFigure::white);
    white.push_back(figure);
    figure = new King(ChessFigure::white);
    white.push_back(figure);
    figure = new Queen(ChessFigure::white);
    white.push_back(figure);
    figure = new Bishop(ChessFigure::white);
    white.push_back(figure);
    figure = new Knight(ChessFigure::white);
    white.push_back(figure);
    figure = new Rook(ChessFigure::white);
    white.push_back(figure);

    for(int i=0; i<8; i++)
    {
        figure = new Pawn(ChessFigure::white);
        white.push_back(figure);
    }

}

void ChessBoard::setBlack()
{
    ChessFigure *figure;

    for(int i=0; i<8; i++)
    {
        figure = new Pawn(ChessFigure::black);
        black.push_back(figure);
    }

    figure = new Rook(ChessFigure::black);
    black.push_back(figure);
    figure = new Knight(ChessFigure::black);
    black.push_back(figure);
    figure = new Bishop(ChessFigure::black);
    black.push_back(figure);
    figure = new King(ChessFigure::black);
    black.push_back(figure);
    figure = new Queen(ChessFigure::black);
    black.push_back(figure);
    figure = new Bishop(ChessFigure::black);
    black.push_back(figure);
    figure = new Knight(ChessFigure::black);
    black.push_back(figure);
    figure = new Rook(ChessFigure::black);
    black.push_back(figure);
}


