#ifndef BISHOP_H
#define BISHOP_H

#include "chessfigure.h"

/*!
 *\brief Klasa odpowiada za reprezentację figury szachowej gońca
 *
 * Klasa odpowiada za stworzenie obiektu klasy Bishop,
 * ustawienie grafik reprezentujących figurę gońca,
 * a także sprawdzanie dozwolonych ruchów w danej sytuacji na planszy
 */
class Bishop : public ChessFigure
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy Bishop
     * \param[in] side kolor figury szachowej
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    Bishop(Color side, QGraphicsItem *parent = 0);

    /*!
     * \brief Funkcja odpowiedzialna za ustawienie grafik reprezentujących
     * figurę gońca
     */
    void setIcon();


    /*!
     * \brief Funkcja odpowiedzialna za sprawdzenie dozwolonych ruchów gońca
     * w danej sytuacji na planszy
     */
    void checkValidMoves();
};

#endif // BISHOP_H
