#include "king.h"
#include "game.h"

extern Game* game;


King::King(Color side, QGraphicsItem *parent):ChessFigure(side, parent)
{
    setIcon();
}


void King::setIcon()
{
    if(this->color==white)
    {
        setPixmap(QPixmap(":/chess_figure_icons/king_white.png"));
    }
    else
    {
        setPixmap(QPixmap(":/chess_figure_icons/king_black.png"));
    }

}

void King::checkValidMoves()
{

    possibleMoves.clear();

    int col = this->currentBox->colPosition;
    int row = this->currentBox->rowPosition;

    int colOffsets[8]={-1,0,+1,+1,+1,0,-1,-1};
    int rowOffsets[8]={-1,-1,-1,0,+1,+1,+1,0};

    for(int i=0;i<8;i++)
    {
        int colToCheck = col + colOffsets[i];
        int rowToCheck = row + rowOffsets[i];

        // if not outside the chessboard
        if(colToCheck>=0 && colToCheck<=7 && rowToCheck>=0 && rowToCheck<=7)
        {
            if((game->fields[colToCheck][rowToCheck]->getCurrentChessFigure() != NULL && game->fields[colToCheck][rowToCheck]->getCurrentChessFigure()->getColor()!=this->getColor()) || game->fields[colToCheck][rowToCheck]->getCurrentChessFigure() == NULL)
            {
                    possibleMoves.push_back(game->fields[colToCheck][rowToCheck]);
            }

        }

    }

}

