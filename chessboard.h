#ifndef CHESSBOARD_H
#define CHESSBOARD_H

#include <QGraphicsRectItem>
#include <vector>
#include "chessfigure.h"

using namespace std;

/*!
 * \brief Klasa odpowiedzialna za reprezentację szachownicy
 *
 * Klasa odpowiada za stworzenie obiektu klasy ChessBoard,
 * narysowanie szachownicy na ekranie oraz wstępne ustawienie figur szachowych
 */

class ChessBoard
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy ChessBoard
     */
    ChessBoard();

    /*!
     * \brief Funkcja odpowiedzialna za narysowanie szachownicy na ekranie
     * \param[in] x współrzędna x górnego lewego rogu szachownicy na ekranie
     * \param[in] y współrzędna y górnego lewego rogu szachownicy na ekranie
     */
    void drawChessBoard(int x, int y);

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektów białych figur
     */
    void setWhite();

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektów czarnych figur
     */
    void setBlack();

    /*!
     * \brief Funkcja odpowiedzialna za wstępne umiejscowienie i wyświetlenie
     * figur szachowych na planszy
     */
    void addChessFigure();

private:

    /*!
     * \brief Tablica zawierająca wszystkie białe figury biorące udział w grze
     */
    vector <ChessFigure *> white;

    /*!
     * \brief Tablica zawierająca wszystkie czarne figury biorące udział w grze
     */
    vector <ChessFigure *> black;

};

#endif // CHESSBOARD_H
