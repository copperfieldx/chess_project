#ifndef CHESSBOX_H
#define CHESSBOX_H

#include <QGraphicsRectItem>
#include <QBrush>
#include "chessfigure.h"
#include <QGraphicsSceneMouseEvent>

class ChessFigure;


/*!
 * \brief Klasa odpowiedzialna za reprezentację pojedynczego pola szachownicy
 *
 * Klasa odpowiada za stworzenie obiektu klasy ChessFigure,
 * zdefiniowanie wyglądu pojedynczego pola szachownicy
 * oraz interakcję na działania użytkownika(klikanie myszką)
 *
 */
class ChessBox:public QGraphicsRectItem
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy ChessBox
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    ChessBox(QGraphicsItem *parent=0);

    /*!
     * \brief Funkcja odpowiedzialna za interakcję obiektu na działania użytkownika
     * (klikanie myszką)
     * \param[in] event
     */
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    /*!
     * \brief Funkcja odpowiedzialna za wstępne ustawienie i wyswietlenie
     * figury szachowej w polu szachownicy
     * \param[in] figure figura szachowa do ustawienia w danym polu szachownicy
     */
    void setFigure(ChessFigure *figure);

    /*!
     * \brief Funkcja odpowiedzialna za zmianę aktualnego koloru danego pola szachownicy
     * \param[in] color kolor pola szachownicy do ustawienia
     */
    void changeColor(QColor color);

    /*!
     * \brief Funkcja odpowiedzialna za ustawienie pierwotnego koloru danego pola szachownicy
     * \param[in] color kolor pola szachownicy do ustawienia
     */
    void setBasicColor(QColor color);

    /*!
     * \brief Funkcja zwracająca pierwotny kolor danego pola szachownicy
     * \return podstawowy kolor danego pola szachownicy
     */
    QColor getBasicColor();

    /*!
     * \brief Funkcja zwracająca aktualną figurę stojącą na danym polu szachownicy
     * \return figura aktualnie stojąca na danym polu szachownicy
     */
    ChessFigure* getCurrentChessFigure();

    /*!
     * \brief Numer kolumny danego pola szachownicy
     */
    int colPosition;

    /*!
     * \brief Numer wiersza danego pola szachownicy
     */
    int rowPosition;

private:

    /*!
     * \brief Kolor danego pola szachownicy
     */
    QBrush brush;

    /*!
     * \brief Informacja czy na danym polu szachownicy stoi jakaś figura
     */
    bool hasChessFigure;

    /*!
     * \brief Figura stojąca na danym polu szachownicy
     */
    ChessFigure* currentFigure;

    /*!
     * \brief Pierwotny kolor danego pola szachownicy
     */
    QColor basicColor;

    /*!
     * \brief Aktualny kolor danego pola szachownicy
     */
    QColor currentColor;
};

#endif // CHESSBOX_H
