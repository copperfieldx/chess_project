#ifndef ROOK_H
#define ROOK_H

#include "chessfigure.h"


/*!
 *\brief Klasa odpowiada za reprezentację figury szachowej wieży
 *
 * Klasa odpowiada za stworzenie obiektu klasy Rook,
 * ustawienie grafik reprezentujących figurę wieżę,
 * a także sprawdzanie dozwolonych ruchów w danej sytuacji na planszy
 */
class Rook : public ChessFigure
{
public:

    /*!
     * \brief Funkcja odpowiedzialna za stworzenie obiektu klasy Rook
     * \param[in] side kolor figury szachowej
     * \param[in] parent element graficzny z którego dziedziczy klasa
     */
    Rook(Color side, QGraphicsItem *parent = 0);

    /*!
     * \brief Funkcja odpowiedzialna za ustawienie grafik reprezentujących
     * figurę wieży
     */
    void setIcon();

    /*!
     * \brief Funkcja odpowiedzialna za sprawdzenie dozwolonych ruchów wieży
     * w danej sytuacji na planszy
     */
    void checkValidMoves();
};

#endif // ROOK_H
