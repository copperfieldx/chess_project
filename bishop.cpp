#include "bishop.h"
#include "game.h"

extern Game* game;


Bishop::Bishop(Color side, QGraphicsItem *parent):ChessFigure(side, parent)
{
    setIcon();
}


void Bishop::setIcon()
{
    if(this->color==white)
    {
        setPixmap(QPixmap(":/chess_figure_icons/bishop_white.png"));
    }
    else
    {
        setPixmap(QPixmap(":/chess_figure_icons/bishop_black.png"));
    }
}

void Bishop::checkValidMoves()
{
    possibleMoves.clear();

    int col = this->currentBox->colPosition;
    int row = this->currentBox->rowPosition;


    //up left
    for(int i=col-1, j=row-1; i>=0 && j>=0; i--,j--)
    {

        if(game->fields[i][j]->getCurrentChessFigure() != NULL)
        {
            if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
            {
                break;
            }
            else
            {
                possibleMoves.push_back(game->fields[i][j]);
                break;
            }

        }
        else
        {
            possibleMoves.push_back(game->fields[i][j]);
        }

    }

    //up right
    for(int i=col+1,j=row-1; i<=7 && j>=0; i++,j--)
    {

        if(game->fields[i][j]->getCurrentChessFigure() != NULL)
        {
            if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
            {
                break;
            }
            else
            {
                possibleMoves.push_back(game->fields[i][j]);
                break;
            }
        }
        else
        {
            possibleMoves.push_back(game->fields[i][j]);
        }

    }

    //down right
    for(int i=col+1,j=row+1; i<=7 && j<=7; i++,j++)
    {

        if(game->fields[i][j]->getCurrentChessFigure() != NULL)
        {
            if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
            {
                break;
            }
            else
            {
                 possibleMoves.push_back(game->fields[i][j]);
                 break;
            }
        }
        else
        {
             possibleMoves.push_back(game->fields[i][j]);
        }

    }


    //down left
    for(int i=col-1,j=row+1; i>=0 && j<=7; i--,j++)
    {

        if(game->fields[i][j]->getCurrentChessFigure() != NULL)
        {
            if(game->fields[i][j]->getCurrentChessFigure()->getColor()==this->getColor())
            {
                break;
            }
            else
            {
                possibleMoves.push_back(game->fields[i][j]);
                break;
            }
        }
        else
        {
            possibleMoves.push_back(game->fields[i][j]);
        }
    }

}
