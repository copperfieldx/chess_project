#-------------------------------------------------
#
# Project created by QtCreator 2019-03-03T09:29:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chess_project
TEMPLATE = app


SOURCES += main.cpp\
        game.cpp \
    chessboard.cpp \
    chessbox.cpp \
    chessfigure.cpp \
    bishop.cpp \
    king.cpp \
    knight.cpp \
    pawn.cpp \
    queen.cpp \
    rook.cpp

HEADERS  += game.h \
    chessboard.h \
    chessbox.h \
    chessfigure.h \
    bishop.h \
    king.h \
    knight.h \
    pawn.h \
    queen.h \
    rook.h

FORMS    += game.ui

RESOURCES += \
    resources.qrc
