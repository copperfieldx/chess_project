#include "chessbox.h"
#include "game.h"
#include <iostream>

using namespace std;

extern Game* game;

ChessBox::ChessBox(QGraphicsItem *parent):QGraphicsRectItem(parent)
{
    //creating the square chess box
    setRect(0,0,100,100);
    setZValue(-1);
    basicColor = Qt::white;
    hasChessFigure = false;
    currentFigure = NULL;
}


void ChessBox::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    //selecting figure to move
    if(game->currentMoveFigure==NULL && currentFigure!=NULL && currentFigure->getColor()==game->getWhichTurn())
    {
        game->currentMoveFigure = this->getCurrentChessFigure();
        currentFigure->setIsChosen(true);
        game->currentMoveFigure->getCurrentBox()->changeColor(Qt::cyan);
        game->currentMoveFigure->checkValidMoves();
        game->currentMoveFigure->showValidMoves();
        return;
    }

    //deselecting figure
    if(game->currentMoveFigure!=NULL && game->currentMoveFigure==currentFigure)
    {
        currentFigure->setIsChosen(false);
        game->currentMoveFigure->getCurrentBox()->changeColor(game->currentMoveFigure->getCurrentBox()->getBasicColor());
        game->currentMoveFigure->hideValidMoves();
        game->currentMoveFigure = NULL;
        return;
    }

    //checking if box is in possibleMoves for current game figure
    bool isPossibleMove=false;
    if(game->currentMoveFigure !=NULL)
    {
        isPossibleMove = game->currentMoveFigure->checkIsInPossibleMoves(this);
    }

    //if figure selected and target place is empty(moving figure)
    if(game->currentMoveFigure !=NULL && currentFigure == NULL && isPossibleMove == true)
    {
        game->currentMoveFigure->setIsChosen(false);
        game->currentMoveFigure->getCurrentBox()->hasChessFigure = false;
        game->currentMoveFigure->getCurrentBox()->changeColor(game->currentMoveFigure->getCurrentBox()->getBasicColor());
        game->currentMoveFigure->getCurrentBox()->currentFigure = NULL;
        setFigure(game->currentMoveFigure);
        game->currentMoveFigure->hideValidMoves();
        game->currentMoveFigure->setIsFirstMove(false);
        game->currentMoveFigure = NULL;
        game->changeWhichTurn();

        return;
    }

    //if oponent in box and box in possibleMoves(beating)
    if(game->currentMoveFigure != NULL && currentFigure != NULL && currentFigure->getColor() != game->currentMoveFigure->getColor() && isPossibleMove == true)
    {
        game->currentMoveFigure->setIsChosen(false);
        game->currentMoveFigure->getCurrentBox()->hasChessFigure = false;
        game->currentMoveFigure->getCurrentBox()->currentFigure = NULL;
        game->currentMoveFigure->getCurrentBox()->changeColor(game->currentMoveFigure->getCurrentBox()->getBasicColor());
        game->currentMoveFigure->setCurrentBox(currentFigure->getCurrentBox());
        game->currentMoveFigure->hideValidMoves();
        game->removeFromScene(currentFigure);
        game->verifyEndOfTheGame(currentFigure);
        currentFigure->setCurrentBox(NULL);
        currentFigure = NULL;
        setFigure(game->currentMoveFigure);
        game->currentMoveFigure = NULL;
        game->changeWhichTurn();
        return;
    }

}

void ChessBox::setFigure(ChessFigure *figure)
{
    //(100-64)/2=18
    figure->setPos(x()+18,y()+18);
    figure->setZValue(1);
    figure->setCurrentBox(this);
    hasChessFigure = true;
    currentFigure = figure;
}


void ChessBox::changeColor(QColor color)
{
    setBrush(color);
    currentColor = color;
}

void ChessBox::setBasicColor(QColor color)
{
    basicColor = color;
}

QColor ChessBox::getBasicColor()
{
    return basicColor;
}

ChessFigure* ChessBox::getCurrentChessFigure()
{
    return currentFigure;
}
