#include "game.h"
#include <QDebug>
#include <QPixmap>
#include <chessboard.h>
#include <chessfigure.h>
#include <king.h>
#include <iostream>

using namespace std;

int gameWidth = 1500;
int gameHeight = 1000;

Game::Game(QWidget *parent):QGraphicsView(parent)
{
    //Creating the Scene
    gameScene = new QGraphicsScene();
    gameScene->setSceneRect(0,0,gameWidth,gameHeight);

    //Setting size of the Game
    setFixedSize(gameWidth,gameHeight);

    setScene(gameScene);
    gameScene->setBackgroundBrush(QBrush(QColor(240,240,180), Qt::SolidPattern));

    currentMoveFigure = NULL;
    whichTurn = ChessFigure::Color::white;

    whichTurnText = new QGraphicsTextItem;
    whichTurnText->setPos(gameWidth-400,50);
    whichTurnText->setZValue(1);
    whichTurnText->setDefaultTextColor(Qt::black);
    whichTurnText->setFont(QFont("",20));
    whichTurnText->setPlainText("Ruch wykonuje gracz:\nBIAŁY");

    gameOverText = new QGraphicsTextItem;
    gameOverText->setPos(gameWidth/2-500,500);
    gameOverText->setZValue(1);
    gameOverText->setDefaultTextColor(Qt::black);
    gameOverText->setFont(QFont("",50));
    gameOverText->setPlainText("Gra zakończona");
}


void Game::displayGame()
{
    chessboard = new ChessBoard();
    chessboard->drawChessBoard(gameWidth/2-500,50);
    chessboard->addChessFigure();

    addToScene(whichTurnText);
}

void Game::addToScene(QGraphicsItem *item)
{
    gameScene->addItem(item);
}

void Game::removeFromScene(QGraphicsItem *item)
{
    gameScene->removeItem(item);
}

void Game::setWhichTurn(ChessFigure::Color color)
{
    whichTurn = color;
}

ChessFigure::Color Game::getWhichTurn()
{
    return whichTurn;
}

void Game::changeWhichTurn()
{
    if(whichTurn == ChessFigure::Color::white)
    {
        setWhichTurn(ChessFigure::Color::black);
        whichTurnText->setPlainText("Ruch wykonuje gracz:\nCZARNY");
    }
    else
    {
        setWhichTurn(ChessFigure::Color::white);
        whichTurnText->setPlainText("Ruch wykonuje gracz:\nBIAŁY");
    }
}


void Game::verifyEndOfTheGame(ChessFigure* figure)
{
    King* tmp = dynamic_cast <King* >(figure);
    //if figure instance of class King
    if(tmp)
    {
        if(figure->getColor()==ChessFigure::Color::white)
        {
            gameOverText->setPlainText("Wygrywa gracz CZARNY");
        }
        else
        {
            gameOverText->setPlainText("Wygrywa gracz BIAŁY");
        }
        addToScene(gameOverText);
        clearAll();
//        removeFromScene(whichTurnText);
    }
}

void Game::clearAll()
{
    QList<QGraphicsItem*> items = gameScene->items();
    for(int i=0; i<items.size(); i++)
    {
        if(items[i]!=gameOverText)
        {
            removeFromScene(items[i]);
        }
    }
}
